<#
 # Used as a pre-stater for my automated Teamviewer-quicksupport-lastest-version-launcher.
#>

# Check if session is elevated, elevate if its not
[bool]$isElevated = (New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)

if ($isElevated)
{
    # Is already elevated, just execute command.
    invoke-webrequest 'https://gitlab.com/jackgreyhat/teamviewerdownloader/-/raw/master/QSDownloader.ps1' -outfile .\QSDownloaderFetched.ps1 ; Invoke-expression -command .\QSDownloaderFetched.ps1
}
else
{
    # Start a new elevated PowerShell session and execute ArgumentList
    # Note that Runas ignores the WorkingDirectory parameter. Therefore, we are doing cd $PSScriptRoot at the start of the ArgumentList...Gotta love :]
	Start-Process -FilePath PowerShell.exe -Verb Runas -ArgumentList "cd $PSScriptRoot; invoke-webrequest 'https://gitlab.com/jackgreyhat/teamviewerdownloader/-/raw/master/QSDownloader.ps1' -outfile .\QSDownloaderFetched.ps1 ; Invoke-expression -command.\QSDownloaderFetched.ps1"
}