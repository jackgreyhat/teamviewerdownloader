# Variables
write-host ''
write-host ''
write-host ''
write-host ''
write-host ''
write-host ''
write-host ''
write-host ''
write-host ''
write-host -ForegroundColor White '                                                         888     d8b           '
write-host -ForegroundColor White '                                                         888     Y8P           '
write-host -ForegroundColor White '                                                         888                   '
write-host -ForegroundColor White '  88888b.d88b.   .d88b.  88888b.d88b.   .d88b.  88888b.  888888 8888  .d88b.   '
write-host -ForegroundColor White '  888 "888 "88b d88  88b 888 "888 "88b d8P  Y8b 888 "88b 888    "888 d8P  Y8b  '
write-host -ForegroundColor White '  888  888  888 888  888 888  888  888 88888888 888  888 888     888 88888888  '
write-host -ForegroundColor White '  888  888  888 Y88  88P 888  888  888 Y8b.     888  888 Y88b.   888 Y8b.      '
write-host -ForegroundColor White '  888  888  888  "Y88P"  888  888  888  "Y8888  888  888  "Y888  888  "Y8888   '
write-host -ForegroundColor White '                                                                 888           '
write-host -ForegroundColor White '                                                                d88P           '
write-host -ForegroundColor White '                                                              888P"            '
write-host -ForegroundColor White '  Dit kan een aantal minuten duren...Geen Paniek!'

Invoke-WebRequest https://download.teamviewer.com/download/TeamViewerQS.exe -OutFile $PSScriptRoot\TeamViewerQS.exe
Start-Process $PSScriptRoot\TeamViewerQS.exe
write-host ''
write-host ''
write-host ''
start-sleep -Seconds 10
Write-Host -Object ' Engage killswitch and close connections?' -ForegroundColor red
Write-Host -NoNewline -Object ' (y/n): ' -ForegroundColor red
$answer = Read-Host

if ($answer -like 'y') {
    start-sleep -Seconds 3
    Get-Process -name Teamviewer | Stop-Process -Force
}
else {
    Write-Host -Object ''
    Write-Host -Object 'To bad!' -ForegroundColor Red
    Write-Host -Object 'Press ENTER to Quit.' -ForegroundColor Red
}